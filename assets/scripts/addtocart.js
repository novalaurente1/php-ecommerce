// we need to get all buttons
let addtocart_buttons = document.querySelectorAll(".addToCartBtn");

// we need to add an event listener to each of the buttons
addtocart_buttons.forEach(addToCartBtn => {
	addToCartBtn.addEventListener('click', indiv_button => {
		// get the data from the button (which is the item_id)
		let id = indiv_button.target.getAttribute("data-id");

		// get the data from the input (which is the item_quantity)
		let quantity = indiv_button.target.previousElementSibling.value;
		console.log(quantity);

		// chech if quantity is < 0, reject 
		// if > 0, send the data via fetch
		if (quantity < 0) {
			alert("Please enter quantity");
		} else {
			let data = new FormData;

			data.append("id", id);
			data.append("quantity", quantity);

			fetch("../../controllers/process_update_cart.php", {
				method: "POST",
				body: data
			}).then(response => {
				return response.text();
			}).then(data_from_fetch => {
				console.log(data_from_fetch);
			})	
		}
	})
})





