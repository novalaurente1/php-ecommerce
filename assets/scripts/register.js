
function validate_reg_form(){
	errors = 0;
	let username = document.querySelector("#username").value;
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#email").value;
	let address = document.querySelector("#address").value;
	let password = document.querySelector("#password").value;
	let confirm = document.querySelector("#confirm").value;

	// no empty fields
	// username must be at least 8 chars long max 24
	// password must be at leat 8 chars long
	// confirm = password
	if (firstName == "") {
		document.querySelector("#firstName").nextElementSibling.innerHTML = "Please provide a valid first name";
		errors++;
	} else {
		document.querySelector("#firstName").nextElementSibling.innerHTML = "";
	};

	if (lastName == "") {
		document.querySelector("#lastName").nextElementSibling.innerHTML = "Please provide a valid last name";
		errors++;
	} else {
		document.querySelector("#lastName").nextElementSibling.innerHTML = "";
	};

	if (email == "") {
		document.querySelector("#email").nextElementSibling.innerHTML = "Please provide a valid email";
		errors++;
	} else {
		document.querySelector("#email").nextElementSibling.innerHTML = "";
	};

	if (address == "") {
		document.querySelector("#address").nextElementSibling.innerHTML = "Please provide a valid address";
		errors++;
	} else {
		document.querySelector("#address").nextElementSibling.innerHTML = "";
	};

	if (username == "") {
		document.querySelector("#username").nextElementSibling.innerHTML = "Please provide a valid username";
		errors++;
	} else {
		document.querySelector("#username").nextElementSibling.innerHTML = "";
	};

	if (password == "") {
		document.querySelector("#password").nextElementSibling.innerHTML = "Please provide a valid password";
		errors++;
	} else {
		document.querySelector("#password").nextElementSibling.innerHTML = "";
	};

	if (confirm == "") {
		document.querySelector("#confirm").nextElementSibling.innerHTML = "Please confirm your password";
		errors++;
	} else {
		document.querySelector("#confirm").nextElementSibling.innerHTML = "";
	};

	if (username.length<8 || username.length>24) {
		document.querySelector("#username").nextElementSibling.innerHTML = "Please input a valid username";
		errors++;
	} else {
		document.querySelector("#username").nextElementSibling.innerHTML = "";
	};

	if (password.length<8) {
		document.querySelector("#password").nextElementSibling.innerHTML = "Please input a valid password";
		errors++;
	} else {
		document.querySelector("#password").nextElementSibling.innerHTML = "";
	};

	if (password!=confirm) {
		document.querySelector("#password").nextElementSibling.innerHTML = "Please input the same password";
		errors++;
	} else {
		document.querySelector("#password").nextElementSibling.innerHTML = "";
	};

	if (errors > 0){
		return false;
	}else{
		return true;
	};

}

document.querySelector('#registerUser').addEventListener("click", ()=>{
	// get the values

	if (validate_reg_form()) {
		let username = document.querySelector("#username").value;
		let firstName = document.querySelector("#firstName").value;
		let lastName = document.querySelector("#lastName").value;
		let email = document.querySelector("#email").value;
		let address = document.querySelector("#address").value;
		let password = document.querySelector("#password").value;

		let data = new FormData;

		data.append("firstName", firstName);
		data.append("lastName", lastName);
		data.append("email", email);
		data.append("address", address);
		data.append("username", username);
		data.append("password", password);

		fetch("../../controllers/process_register_user.php", {
			method: "POST",
			body: data
		}).then( response => {
			return response.text();
		}).then(data_from_fetch => {
			console.log(data_from_fetch);
		})
	}
})