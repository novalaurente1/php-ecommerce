<!-- DELETE: 

1. We need to send somthing (id) for the database to know which item to delete.
2. We'll send the id via url.
3. Create an anchor tag in the catalog - card footer together with the edit button and in the href include ?id=#input id here via #php
4. Create the delete controller -->
<?php 
	
	require "connection.php";

// 5. In the controller, capture the id via GET.
	$id = $_GET['id'];

// 6. Write your delete query: important don't forget WHERE 
	$delete_item_query = "DELETE FROM items WHERE id = $id";
	
// 7. Use mysqli_query
	$result = mysqli_query($conn, $delete_item_query);

// 8. Go back to where we came from
	header("Location: ".$_SERVER['HTTP_REFERER']);

 ?>