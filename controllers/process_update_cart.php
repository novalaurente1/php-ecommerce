<?php 
	session_start();
	// we'll capture the item_id

	$item_id = $_POST['id'];
	// we'll capture the quantity
	$item_quantity = $_POST['quantity'];	

	// 1. check if the id/quantity is coming from the cart page
	// if yes, set the $_SESSION['cart'][$item_id] = to the quantity received
	// if not, (else) use the code below

	if (isset($_POST['fromCartPage'])) {
		$_SESSION['cart'][$item_id] = $item_quantity;
		header("LOCATION:". $_SERVER['HTTP_REFERER']);
	} else {
		// if first time, save the item_id and quantity in a session variable
		// if not, just increase the quantity
		if (isset($_SESSION['cart'][$item_id])) {
			$_SESSION['cart'][$item_id] += $item_quantity;
		} else {
			$_SESSION['cart'][$item_id] = $item_quantity;
		}
		echo print_r($_SESSION['cart']);
	}


	
 ?>