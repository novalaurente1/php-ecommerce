<?php 
	require "../templates/template.php";
	function get_content(){
		require '../controllers/connection.php';
?>
	<h1 class="text-center py-5">Catalog</h1>

	<div class="container">
		<div class="row">
			<!-- sidebar -->
			<div class="col-lg-2">
				<h3>Categories</h3>
				<ul class="list-group border">
					<li class="list-group-item">
						<a href="catalog.php">All</a>
					</li>
					<!-- 1. just below the ALL <li>, call all categories -->
					<?php
						// a. write our select query
						$categories_query = "SELECT * FROM categories";
						// b. use mysqli_query
						$categoryList = mysqli_query($conn, $categories_query);
						// c. use foreach to display each category in an <li>#insert category name via php#</li>
						foreach ($categoryList as $indiv_category) {
							?>
								<!-- lets create an anchor tag inside the <li> where we will put the category name -->
								<!-- href should be refering to catalog.php?category_id = #input here via php# -->
								<li class="list-group-item"> <a href="catalog.php?category_id=<?php echo $indiv_category['id'] ?>"><?php echo $indiv_category['name'] ?></a></li>
							<?php
						}

					 ?>
				</ul>
				<!-- sorting -->
				<h3>Sort By</h3>
				<ul class="list-group border">
					<li class="list-group-item">
						<a href="../controllers/process_sort.php?sort=asc">Price (Lowest to Highest)</a>
					</li>
					<li class="list-group-item">
						<a href="../controllers/process_sort.php?sort=desc">Price (Highest to Lowest)</a>
					</li>
				</ul>
			</div>
			<!-- item list -->
			<div class="col-lg-10">
				<div class="row">
					<?php 

					// REVIEW THIS LATER
					session_start();

					// steps for retrieving items
					// 1. create a query
					$items_query = "SELECT * FROM items";

					// for filtering
					if (isset($_GET['category_id'])) {

						$catId = $_GET['category_id'];

						$items_query .= " WHERE category_id = $catId";
					}

					// sort
					if (isset($_SESSION['sort'])) {
						$items_query .= $_SESSION['sort'];
					}

					// 2. use mysqli_query to get the results
					$items = mysqli_query($conn, $items_query);
					
					// 3. if array (use foreach)
					foreach ($items as $indiv_item) {
						?>
							<div class="col-lg-4 py-2">
								<div class="card">
									<img class="card-img-top" height="250px" src="<?php echo $indiv_item['image'] ?>" alt="image">
									<div class="card-body">
										<h4 class="card-title"><?php echo $indiv_item['name'] ?></h4>
										<p class="card-text">Php <?php echo $indiv_item['price'] ?>.00</p>
										<p class="card-text"><?php echo $indiv_item['description'] ?></p>
										<?php 
											// process of displaying category
											// 1. get the category name where id is equal to $indiv_item['category_id']
											$catId = $indiv_item['category_id'];
											$category_query = "SELECT * FROM categories WHERE id = $catId";
											// 2. display the data

											$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
										 ?>
										 <p class="card-text">Category: <?= $category['name'] ?></p>
									</div>
									<div class="card-footer">
										<a href="edit_item_form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-info">Edit Item</a>
										<a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Delete Item</a>
									</div>
									<div class="card-footer">
										<input type="number" class="form-control" value="1">
										<button type="button" class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id']?>">Add To Cart</button>
									</div>
								</div>
							</div>
						<?php
					}
					// 4. if object (use mysqli_fetch_assoc to convert the data to an associative array)
					// 4.1 use foreach ($result as $key => $value)
				 	?>
			 	</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="../assets/scripts/addtocart.js"></script>

<?php
	}

 ?>