<?php 

	require "../templates/template.php";

	// REVIEW THIS LATER
	
	function get_content(){
		session_start();
		require "../controllers/connection.php";
		?>
		
		<h1 class="text-center py-5">Cart</h1>
		<hr>

		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr class="text-center">
						<th>Item</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 

						$total = 0;
						// 1. check whether we have $_SESSION CART
						if (isset($_SESSION['cart'])) {
						// 2. get the session cart, and display each in a <tr>
							// a. check the data type of our $_SESSION CART
							// ANSWER: use var_dump, answer is associative array
							// b. do the loop ( foreach ($data as $key => $value))
							foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
								// c. the goal is to get the id and the quantity
								// d. get the item details through item_query
								// e. select * from items where id = the id we got from the foreach
								$item_query = "SELECT * FROM items WHERE id = $item_id";
								// f. use mysqli_query to get individual item
								$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
								// g. get the subtotal by multipying indiv_item['price'] and quantity we got from the forach
								$subtotal = $indiv_item['price']*$item_quantity;
								// get the total (in order for us to do so, we must initialize $total at the very top)
								$total += $subtotal;
								?>
									
								<!-- display the details in <tr><td> -->
									<tr>
                                       <td><?php echo $indiv_item['name'] ?> </td>
                                       <td><?php echo $indiv_item['price'] ?> </td>
                                       <td>
                                           <form action="../controllers/process_update_cart.php" method="POST">
                                               <input type="hidden" name="fromCartPage" value="true">
                                               <input type="hidden" name="id" value="<?php echo $item_id?>">
                                               <input type="number" name="quantity" value="<?php echo $item_quantity?>" class="form-control quantityInput">
                                           </form>
                                       </td>
                                       <td><?php echo $subtotal ?> </td>
                                       <td><a href="../controllers/process_remove_item.php?id=<?php echo $indiv_item['id']?>" class="btn btn-danger">Remove ITEM</a></td>
                                   </tr>

								<?php
							}
						}
							
					 ?>
					 <tr>
					 	<td></td>
					 	<td></td>
					 	<td><a href="../controllers/process_empty_cart.php" class="btn btn-danger">Empty Cart</a></td>
					 	<td>Total: <?php echo $total?></td>
					 	<td></td>
					 </tr>
				</tbody>
			</table>
		</div>
		<script src="../assets/scripts/updatecart.js"></script>
		<?php
	}

 ?>